import React from 'react';
//import { Link } from 'react-router-dom'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button'
import { TextField, Grid, Paper, Container, Link } from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { Redirect } from 'react-router-dom'


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  reg: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '20px',
    paddingBottom: '40px',
    '& .MuiTextField-root, .MuiButton-root': {
      margin: theme.spacing(1),
    }
  }
}));

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#424242',
    } 
  },
  typography: {
    subtitle1: {
      fontSize: 12,
    },
    body1: {
      fontWeight: 500,
    },
  },
});



function SignIn() {
  const classes = useStyles();

  return (
    <div className="App">
      <ThemeProvider theme={theme}>
      <Typography variant="h1" className={classes.root} gutterBottom>S4S</Typography>
       <Container maxWidth="xs" >
         
       <Paper elevation={3} className={classes.reg}>
        <Typography variant="h4" className={classes.root} gutterBottom>Войти</Typography>
        <Grid 
          container
          direction="column"
          justify="center"
          alignItems="center"
        >
          <TextField 
            label="Emal"
            type="helper text"
            autoComplete="current email"

          />
          <TextField 
            label="Пароль"
            type="password"
            autoComplete="current password"
          />
          
          <Grid container direction="column" justify="center" alignItems="center" spacing={2}>
            <Button variant="contained" color="primary" onClick={()=> (<Redirect from='signin' to='/user'/>)} gutterBottom>Войти</Button>  
            
            
            <Typography variant="subtitle1" >
                <Grid container direction="column" justify="center" alignItems="center" spacing={2}>
                    <Link href ="/registration" color="default" underline="hover">Зарегистрироваться на S4S</Link>
                    <Link href ="/recover-password" color="default" underline="hover">Забыли пароль?</Link>
                </Grid>
            </Typography>
            
          </Grid>
            
        </Grid>
         
      </Paper>
      </Container>
      </ThemeProvider>
    </div>
  );
}


export default SignIn;
