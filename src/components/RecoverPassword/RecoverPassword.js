import React from 'react';
//import { Link } from 'react-router-dom'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button'
import { TextField, Grid, Paper, Container, Link } from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles(theme => ({
    root: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      textAlign: 'center',
    },
    reg: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      padding: '20px',
      paddingBottom: '40px',
      '& .MuiTextField-root, .MuiButton-root': {
        margin: theme.spacing(1),
      }
    }
  }));
  
  const theme = createMuiTheme({
    palette: {
      primary: {
        main: '#424242',
      } 
    },
    typography: {
      subtitle1: {
        fontSize: 12,
      },
      body1: {
        fontWeight: 500,
      },
    },
    textfield: {
      fontSize: 8,
    },
  });
  

function RecoverPassword() {
    const classes = useStyles();
  
    return (
      <div className="App">
        <ThemeProvider theme={theme}>
        <Typography variant="h1" className={classes.root} gutterBottom>S4S</Typography>
         <Container maxWidth="xs" >
           
         <Paper elevation={3} className={classes.reg}>
          <Typography variant="h6" className={classes.root} gutterBottom>Восстановление пароля</Typography>
          
          <Grid 
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Typography variant="subtitle1" className={classes.root} maxWidth="sm"  gutterBottom>Для восстановления доступа укажите ник, e-mail или номер телефона, привязанные к вашему профилю</Typography>
            <TextField 
              label="Ник, email или номер телефона "
              type="helper text"
              autoComplete="current email"
              size="medium"
              style={{minWidth: "300px"}}
            />  
            
            <Button variant="contained" color="primary" gutterBottom>Далее</Button>  
              
        </Grid>
           
        </Paper>
        </Container>
        </ThemeProvider>
      </div>
    );
  }
  
  export default RecoverPassword;