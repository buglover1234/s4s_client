import React, { Component } from 'react';

import {
  Route, 
  Switch,
  Redirect,
  withRouter
} from "react-router-dom"

import SignIn from './components/SignIn/SignIn'
import Registration from './components/Registration/Registration';
import RecoverPassword from './components/RecoverPassword/RecoverPassword';

class App extends Component {
  render() {
    const { history } = this.props

    return (
      <div className="App">
        <Switch>
          <Route history={history} path='/registration' component={Registration} />
          <Route history={history} path='/signin' component={SignIn} />
          <Route history={history} path='/recover-password' component={RecoverPassword} />
          <Redirect from='/' to='/registration'/>
        </Switch>
      </div>
    );
  }
}

export default withRouter(App)